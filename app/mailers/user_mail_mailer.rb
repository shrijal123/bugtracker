class UserMailMailer < ApplicationMailer
  default from: 'shirjaltamrakar@gmail.com'

  def welcome_email
    @user = params[:user]
    @url = 'http://example.com/login'
    mail(to: @user.email, subject: 'welcome to my Awesome Site')
  end
end
