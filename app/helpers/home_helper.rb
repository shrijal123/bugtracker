module HomeHelper

  def dashboard_menu_options
    links = ""

    if current_user.admin?
      links += link_to "Invite User", new_user_invitation_path
      links += link_to "Users", users_path
    end
    links += link_to "Projects", projects_path 
    links += link_to "Sign Out", destroy_user_session_path, :method => :delete
    links
  end
end
