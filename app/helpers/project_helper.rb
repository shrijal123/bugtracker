module ProjectHelper

  def show_emails
    link = ""
    @project.users.map do |i|
      link +=i.email + "<br>"
    end
    link.html_safe
  end

end
