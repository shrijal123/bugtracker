class BugsController < ApplicationController
  before_action :project_id
  def index
    @bugs=Bug.where(:project_id => params[:project_id])
  end

  def show
    @bug=Bug.find(params[:id])
    
  end

  def new
    @project = Project.find(params[:project_id])
    @bug = @project.bugs.new
    @project_id=params[:project_id]
  end

  def create
    @project = Project.find(params[:project_id])
    @bug = @project.bugs.new
    if @bug.save
      redirect_to(projects_path)
    else
      render('new')
    end
  end

  def edit
    @project = Project.find(params[:project_id])
    @bug = @project.bugs.find(params[:id])
  end

  def update
    @bug = Bug.find(params[:id])
    if @bug.update(bug_params)
      redirect_to(project_bugs_path(@project))
    else
      render('edit')
    end
  end

  def delete
    @bug=Bug.find(params[:id])
  end

  def destroy
    @bug=Bug.find(params[:id])
    @bug.destroy
    redirect_to(projects_path)
  end

  def assign
    @bug = Bug.find(params[:id])
    @bug.user = current_user
    if @bug.save
      redirect_to(project_bugs_path(@project_id))
    else
      render('edit')
    end
  end

  private
  def bug_params
    params.require(:bug).permit(:bug_name, :bug_description, :project_id, :user, :status)
  end

  def project_id
    @project_id = params[:project_id]
  end
end
