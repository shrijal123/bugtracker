class ProjectsController < ApplicationController

  before_action :authenticate_user!

  def index
      @project = Project.all
  end

  def show
    @project = Project.find(params[:id])
  end
  
  def new
    @project = Project.new
    @users = User.all
  end

  def create
    @project = Project.new(project_params)
    # binding.pry
    users = User.where(id: params[:project][:u_id])
    @project.users << users
    if @project.save
      flash[:notice] = "Project created"
      redirect_to(projects_path)
    else
      render('new')
    end
  end

  def edit
    @project=Project.find(params[:id])
  end

  def update
    @project=Project.find(params[:id])
    users = User.where(id: params[:project][:u_id])
    old_users = @project.users
    old_users << users
    @project.users = old_users.uniq

    if @project.update(project_params)
      redirect_to(project_path(@project))
    else
      render('edit')
    end
  end

  def delete
    @project=Project.find(params[:id])
  end

  def destroy
    @project= Project.find(params[:id])
    @project.destroy
    redirect_to(projects_path)
  end

  private
  
  def project_params
    params.require(:project).permit(:project_name, :project_description)
  end

end
