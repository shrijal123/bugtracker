class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :registerable, :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_and_belongs_to_many :projects

  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :validatable

end
