class Bug < ApplicationRecord
  enum status: [ :solved, :unsolved, :handling, :ignored]

  belongs_to :project
  has_and_belongs_to_many :users
end
