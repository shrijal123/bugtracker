class CreateBugs < ActiveRecord::Migration[5.2]
  def change
    create_table :bugs do |t|
      t.string "bug_name"
      t.integer "project_id"
      t.string "bug_description"
      t.integer "status"
      t.timestamps
    end
    
    add_index("bugs", "project_id")
    
  end
end
