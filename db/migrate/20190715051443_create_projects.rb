class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string "project_name"
      t.integer "user_id"
      t.string "project_description"
      t.timestamps
    end
    
    add_index("projects", "user_id")
    
  end
end
