class AddUserToBugs < ActiveRecord::Migration[5.2]
  def change
    add_column :bugs, :user, :string
  end
end
