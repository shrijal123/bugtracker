Rails.application.routes.draw do
  devise_for :users
  
  authenticated :user do
    root to: "home#dashboard", as: :authenticated_root
  end
  
  resources :users do
    member do
      get :delete
    end
  end

  resources :projects do

    resources :bugs do
      get :delete
    end
    put '/bugs/:id/assign', to: 'bugs#assign', as: 'assign_bug'

    member do
      get :delete
    end
  end
  
  # resources :bugs do
  #   member do
  #     get :delete
  #   end
  # end
  
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # root to: "home#index"
  
end
